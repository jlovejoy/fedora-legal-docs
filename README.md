# Fedora legal documentation

## Copyright and license information

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0
